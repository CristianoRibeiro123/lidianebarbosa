<!doctype html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Lidiane Barbosa</title>

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Raleway:100,300,400,600,700" rel="stylesheet" type="text/css">
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">

    <!-- Styles -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/css/bootstrap.min.css"
          integrity="sha384-PsH8R72JQ3SOdhVi3uxftmaW6Vc51MKb0q5P2rRUpPvrszuE4W1povHYgTpBfshb" crossorigin="anonymous">

    <link rel="stylesheet" href="{{asset('css/style.css')}}">
   
   
</head>
<body>
<section >



    <div class="container">
                          <!-- Video BG Init -->
     
            <!--<div class="row font-times pt-4">
                
                    <div class="col-sm-12 col-md-4 col-lg-4 title-logo">
                    <a class="link-nav" href="#">LOREM IPSUM <span class="sr-only">(current)</span></a>
                    </div>
                    
                    

                    <div class="col-sm-12 col-md-4 col-lg-4 text-center img-logo">
                        <img src="{{asset('img/logo_lidianebarbosa_peq.png')}}" alt="Lidiane Barbosa">
                    </div>
                    <div class="col-sm-12 col-md-4 col-lg-4 text-right button-nav">
                        <button class="btn btn-outline-secondary my-2 my-sm-0" type="submit">LOGIN</button>
                    </div>
            </div>-->
            <nav class="navbar navbar-expand-lg navbar-light font-times">
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarTogglerDemo02"
                    aria-controls="navbarTogglerDemo02" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>

            <div class="collapse navbar-collapse" id="navbarTogglerDemo02">
                <ul class="navbar-nav col-md-4">
                    <li class="nav-item">
                        <a class="nav-link" href="#">LOREM IPSUM <span class="sr-only">(current)</span></a>
                    </li>
                </ul>
                <a class="navbar-brand col-md-4 text-center" href="#">
                    <img src="{{asset('img/logo_lidianebarbosa_peq.png')}}" alt="Lidiane Barbosa">
                </a>
                <ul class="navbar-nav justify-content-end col-md-4">
                    <li class="nav-item">
                        <a href="{{ url('/')}}/login" class="btn btn-outline-secondary my-2 my-sm-0">LOGIN</a>
                    </li>
                </ul>
            </div>
        </nav>



      
            
</section>  



<section>
    
    <div class="container">
        <div class="row">
            <div class="col-12 m-5 p-5">
                <div class="row">
                    <div class="col-5">
                    <h3 class="text-center">Efetue o login</h3>
                        <br>
                        <button class="btn btn-facebook btn-block "> <i class="fa fa-facebook" aria-hidden="true"></i> Logar com o FACEBOOK</button>
                            <br><br>
                            <div class="titulo">
                              <span>
                                OU
                              </span>
                            </div><br>
                    <form action="">
                         <div class="form-group">

                            <label>Email </label>
                            <input type="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Enter email">
                          
                          </div>
                          <div class="form-group">
                            <label>Password</label>
                            <input type="password" class="form-control" id="exampleInputPassword1" placeholder="Password">
                          </div>
                          
                          <button type="submit" class="btn btn-block btn-success">Login</button>
                    </form>
                </div>
                <div class="col-5 border-left">
                    <h3 class="text-center">Faça seu cadastro</h3>

                    <form action="">
                        <div class="form-group mt-5 pt-5">
                            <label>Email </label>
                            <input type="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Enter email">
                            
                          </div>
                          <button type="submit" class="btn btn-block btn-danger">Cadastra-se</button>
                    </form>


                </div>
                </div>
                
            </div>
        </div>
    </div>
</section>

<footer>
    <div class="container">
        <div class="row text-center">
            <div class="col-md-6 col-center">
                <ul class="menu-footer ">
                    <li class="border-gray-right"><a href="#">HOME</a></li>
                    <li class="border-gray-right"><a href="#">CONTATO</a></li>
                    <li class="border-gray-right"><a href="#">TERMOS</a></li>
                    <li class="border-gray-right"><a href="#">PRIVACIDADE</a></li>
                    <li><a href="#">COMPRE AGORA</a></li>
                </ul>
            </div>
            <div class="col-12 mt-5 color-gray">
                <p>DESENVOLVIDO COM <img src="{{ url('/')}}/img/heart.png" alt="AMOR"> POR GRUPO GLADIUM</p>
            </div>
            

            
        </div>
    </div>
</footer>


 <!-- Scripts -->
<script src="http://ajax.googleapis.com/ajax/libs/jquery/3.0.0/jquery.min.js"></script>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.3/umd/popper.min.js"
            integrity="sha384-vFJXuSJphROIrBnz7yo7oB41mKfc8JzQZiCq4NCceLEaO4IHwicKwpJf9c9IpFgh"
            crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/js/bootstrap.min.js"
            integrity="sha384-alpBpkh1PFOepccYVYDB4do5UnbKysX5WZXm3XxPqe5iKTfUKjNkCk9SaVuEZflJ"
            crossorigin="anonymous"></script>
            <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAZsDkJFLS0b59q7cmW0EprwfcfUA8d9dg"></script>


    


</body>
</html>
