<!doctype html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Lidiane Barbosa</title>

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Raleway:100,300,400,600,700" rel="stylesheet" type="text/css">

    <!-- Styles -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/css/bootstrap.min.css"
          integrity="sha384-PsH8R72JQ3SOdhVi3uxftmaW6Vc51MKb0q5P2rRUpPvrszuE4W1povHYgTpBfshb" crossorigin="anonymous">

    <link rel="stylesheet" href="{{asset('css/style.css')}}">
    <link href="{{url('/')}}/css/jquery.mb.YTPlayer.min.css" media="all" rel="stylesheet" type="text/css">
   
</head>
<body>
<section id="banner" style="position: relative;" data-background="img/bg-branco.png" >



    <div class="container">
                          <!-- Video BG Init -->
     
            <!--<div class="row font-times pt-4">
                
                    <div class="col-sm-12 col-md-4 col-lg-4 title-logo">
                    <a class="link-nav" href="#">LOREM IPSUM <span class="sr-only">(current)</span></a>
                    </div>
                    
                    

                    <div class="col-sm-12 col-md-4 col-lg-4 text-center img-logo">
                        <img src="{{asset('img/logo_lidianebarbosa_peq.png')}}" alt="Lidiane Barbosa">
                    </div>
                    <div class="col-sm-12 col-md-4 col-lg-4 text-right button-nav">
                        <button class="btn btn-outline-secondary my-2 my-sm-0" type="submit">LOGIN</button>
                    </div>
            </div>-->
            <nav class="navbar navbar-expand-lg navbar-light font-times">
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarTogglerDemo02"
                    aria-controls="navbarTogglerDemo02" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>

            <div class="collapse navbar-collapse" id="navbarTogglerDemo02">
                <ul class="navbar-nav col-md-4">
                    <li class="nav-item">
                        <a class="nav-link" href="#">LOREM IPSUM <span class="sr-only">(current)</span></a>
                    </li>
                </ul>
                <a class="navbar-brand col-md-4 text-center" href="#">
                    <img src="{{asset('img/logo_lidianebarbosa_peq.png')}}" alt="Lidiane Barbosa">
                </a>
                <ul class="navbar-nav justify-content-end col-md-4">
                    <li class="nav-item">
            <a href="{{ url('/')}}/login" class="btn btn-outline-secondary my-2 my-sm-0">LOGIN</a>
                    </li>
                </ul>
            </div>
        </nav>


        <!-- Video BG Init -->
            <div id="bgndVideo" class="player" data-property="{videoURL:'http://youtu.be/I6jmZ5plZ3o',containment:'#banner',autoPlay:true, showControls:false, showYTLogo: false, mute:true, startAt:0, opacity:1}"> 
            </div>
             <!-- End Video BG Init -->

            
       <!-- Button trigger modal -->


            <div class="row" style="position: relative;
    bottom: 0;
    right: 0;
    left: 0; top: 35vh;">
                <div class="col-md-12 text-center titulo-banner">
                    <img src="img/play-youtube.png" alt="" data-toggle="modal" data-target="#myModal" class="img-fluid mb-5">
                    <h1 class="font-times color-brown">Lidiane Barbosa</h1>
                    <p class="font-raleway-300">GASTRONOMIA FUNCIONAL</p>
                </div>
            </div>

    </div>
    
      
            
</section>  


  <!-- Modal -->
  <div class="modal fade" id="myModal" role="dialog">
     <button type="button" class="close" data-dismiss="modal">&times;</button>
 
    <div class="modal-dialog modal-lg">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
         
        </div>
        <div class="modal-body">

            <div align="center" class="embed-responsive embed-responsive-16by9">
                <!-- autoplay loop -->
                <video  class="embed-responsive-item" controls >
                    <source  src="video/minions.mp4" type="video/mp4">
                </video>
            </div>
        </div>
        
      </div>
      
    </div>
  </div>











<section  class="bg-branco" style="margin-top: 5vh;">
    <div class="container">
        
      
        <div class="row">

            <div class="col-md-4 text-center box-venda border-rigth-roxa">
                <img src="{{asset('img/icon-video.png')}}" alt="Video">
                <h1 class="font-times color-brown">32 Vídeos</h1>
                <p class="font-raleway-500">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ad dolor modi
                    odit!</p>
            </div>
            <div class="col-md-4 text-center box-venda border-rigth-roxa">
                <img src="{{asset('img/icon-chapeu.png')}}" alt="Video">
                <h1 class="font-times color-brown">Conteúdo Exclusivo</h1>
                <p class="font-raleway-500">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ad dolor modi
                    odit!</p>
            </div>
            <div class="col-md-4 text-left box-venda">
                <div class="row">
                    <div class="col-md-6 preco-curso font-times color-brown text-center">
                        <span class="menor">R$</span>
                        <span class="maior">99</span>
                        <span class="menor">,00</span>
                    </div>
                    <div class="col-md-6 pull-right">
                        <p class="color-brown font-raleway-300 text-desc">
                            Lorem ipsum dolor sit amet
                        </p>
                    </div>
                </div>
                <div class="row">
                    <div class="col text-center">
                        <button class="btn btn-primary btn-lg col-12">
                            SALA DE AULA
                        </button>
                    </div>
                </div>
            </div>

        </div>
    </div>
  
        
</section>

<section class="bg-branco  mt-5">
    <div class="container">
        <div class="row">
            <div class="col-12 box-sobre">
                <div class="col-sm-12 col-md-8 col-lg-8">
                    <h1 class="font-times color-brown font-extra-bold">
                        Lorem ipsum dolor sit amet
                    </h1>
                    <h5 class="font-raleway-300">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aliquid
                        beatae blanditiis consequuntur esse est expedita facere hic ipsa, ipsum labore maiores molestias
                        obcaecati odit pariatur, quam quas quisquam sapiente tempora.</h5>
                    <h5 class="font-raleway-300">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aliquid
                        beatae blanditiis consequuntur esse est expedita facere hic ipsa, ipsum labore maiores molestias
                        obcaecati odit pariatur, quam quas quisquam sapiente tempora.</h5>
                </div>
            </div>
        </div>
    </div>
</section>


<section class="bg-branco">
    <div class="container">
        <div class="row">
            <div class="col-12 bg-image">
                <div class="col-12 text-center ">
                   <div class="row pl-md-4 pl-sm-0">
                            <div class="col-sm-12 col-md-3 col-lg-3 bg-box ml-sm-0 ml-md-5 ">
                                <img src="{{url('/')}}/img/play.png" alt="">
                                <h4 class="mt-2 font-times color-brown">32 Video-aulas</h4>
                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit.</p>
                            </div>

                            <div class="col-sm-12 col-md-3 col-lg-3 bg-box ml-sm-0 ml-md-5">
                                <img src="{{url('/')}}/img/book.png" alt="">
                                <h4 class="mt-2 font-times color-brown">Material</h4>
                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit.</p>
                            </div>

                            <div class="col-sm-12 col-md-3 col-lg-3 bg-box ml-sm-0 ml-md-5">
                                 <img src="{{url('/')}}/img/suporte.png" alt="">
                                <h4 class="mt-2 font-times color-brown">Dúvidas</h4>
                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit.</p>
                            </div>
                    </div>
                    

                </div>
            </div>
        </div>
    </div>
</section>

<section class="bg-section-down">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="col-12">
                   <div class="row">
                            <div class="col-sm-12 col-md-2 col-lg-2 offset-md-2 offset-sm-0">
                                
                                <h2 class="mt-2 mt-sm-5 font-times color-brown" > <b>E-Book <br> Exclusivo</b></h2>
                                <span class="text-small">Download grátis</span>
                            </div>

                            <div class="col-sm-12 col-md-3 col-lg-3 text-center">
                                <img src="{{url('/')}}/img/livro.png" class="img img-fluid" alt="">
                               
                            </div>

                            <div class="col-md-3 ml-md-5 mt-md-5 ml-sm-0 text-center">
                                 
                                 <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. adipisicing elit.</p>
                                <button class="btn btn-lg btn-green font-times mt-5">BAIXAR AGORA</button>
                                
                            </div>
                    </div>
                    

                </div>
            </div>
        </div>
    </div>
</section>



<section class="bg-section-depoiment mt-5">
    <div class="container">
        <div class="row">
            
                <div class="col-9 bg-branco col-center text-center">
                    <div class="row">
                        <div class="col-sm-12 col-md-8 col-lg-8 p-sm-0 p-md-4 p-lg-4">
                         <h2 class=" font-times color-brown mt-5">
                            <b> “Os vídeos da Lidi me
                                fizeram ter um  outro olhar
                                sobre a culinária saudável.” </b>
                         </h2>
                        </div>
                        <div class="col-sm-12 col-md-4 col-lg-4">
                            <img src="{{url('/')}}/img/perfil.png" alt="Perfil" class="img img-cicle">
                            <br>
                            <div class="col-8 col-center border-gray-top mt-2"> &nbsp; </div>
                            <p class="color-gray text-small">
                                BATISTA 
                                <br>
                                LOREM IPSIUM LOREN 
                            </p>
                            
                        </div>
                    </div>
                </div>


                <div class="col-10 mt-5 col-center">
                    <div class="row text-center">
                        <div class="col-sm-12 col-md-4 col-lg-4 p-5 border-gray-right">
                            <h1 class="font-times color-brown font-extra-bold">100%</h1>
                            <h3 class="font-times color-brown font-extra-bold mt-4">Garantido </h3>
                            <p>
                                Lorem ipsum dolor sit amet, consectetur.
                            </p>
                        </div>
                        <div class="col-sm-12 col-md-4 col-lg-4 p-5 border-gray-right">
                            <img src="{{url('/')}}/img/screen.png" class="img img-responsive" alt="">

                            <h3 class="font-times color-brown font-extra-bold mt-4">Compatibilidade </h3>
                            <p>
                                Lorem ipsum dolor sit amet, consectetur.
                            </p>
                        </div>
                        <div class="col-sm-12 col-md-4 col-lg-4 p-5">
                             <img src="{{url('/')}}/img/acesso.png" class="img img-responsive" alt="">
                            <h3 class="font-times color-brown font-extra-bold mt-4">Acesso Ilimitado </h3>
                            <p>
                                Lorem ipsum dolor sit amet, consectetur.
                            </p>
                        </div>
                    </div>
                </div>
            
           
        </div>
    </div>
     <img src="{{ url('/')}}/img/line.png" class="img-fluid mt-5" alt="">
</section>



<section class="bg-branco mt-3">
    <div class="container">
        <div class="row">


                <div class="col-12 mt-5 col-center">
                    <div class="row text-center">
                        <div class="col-sm-12 col-md-4 col-lg-4 p-5 border-gray-right">

                             <h3 class="font-times color-brown font-extra-bold mt-4">REDES SOCIAIS </h3>
                            <br>
                            <img src="{{url('/')}}/img/instagram.png" class="img" alt="">
                            <img src="{{url('/')}}/img/facebook.png" class="img" alt="">
                            <img src="{{url('/')}}/img/youtube.png" class="img" alt="">
                            
                           <p class="mt-4">
                                Lorem ipsum dolor sit amet, consectetur.
                            </p>

                    
                        </div>
                        <div class="col-sm-12 col-md-4 col-lg-4 p-5 border-gray-right">
                            

                            <h3 class="font-times color-brown font-extra-bold mt-4">SISTEMA SEGURO </h3>
                            <br>
                            <img src="{{url('/')}}/img/seguranca.png" class="img" alt="">
                            
                            
                           <p class="mt-4">
                                Lorem ipsum dolor sit amet, consectetur.
                            </p>


                        </div>
                        <div class="col-sm-12 col-md-4 col-lg-4 p-5">
                            <h3 class="font-times color-brown font-extra-bold mt-4">CONTATO </h3>
                            <br>
                            <img src="{{url('/')}}/img/email.png" class="img" alt="">
                            
                            
                           <p class="mt-4">
                                Lorem ipsum dolor sit amet, consectetur.
                            </p>
                        </div>
                    </div>
                </div>
            
           
        </div>
    </div>
</section>

<footer>
    <div class="container">
        <div class="row text-center">
            <div class="col-md-6 col-center">
                <ul class="menu-footer ">
                    <li class="border-gray-right"><a href="#">HOME</a></li>
                    <li class="border-gray-right"><a href="#">CONTATO</a></li>
                    <li class="border-gray-right"><a href="#">TERMOS</a></li>
                    <li class="border-gray-right"><a href="#">PRIVACIDADE</a></li>
                    <li><a href="#">COMPRE AGORA</a></li>
                </ul>
            </div>
            <div class="col-12 mt-5 color-gray">
                <p>DESENVOLVIDO COM <img src="{{ url('/')}}/img/heart.png" alt="AMOR"> POR GRUPO GLADIUM</p>
            </div>
            

            
        </div>
    </div>
</footer>


 <!-- Scripts -->
<script src="http://ajax.googleapis.com/ajax/libs/jquery/3.0.0/jquery.min.js"></script>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.3/umd/popper.min.js"
            integrity="sha384-vFJXuSJphROIrBnz7yo7oB41mKfc8JzQZiCq4NCceLEaO4IHwicKwpJf9c9IpFgh"
            crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/js/bootstrap.min.js"
            integrity="sha384-alpBpkh1PFOepccYVYDB4do5UnbKysX5WZXm3XxPqe5iKTfUKjNkCk9SaVuEZflJ"
            crossorigin="anonymous"></script>
            <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAZsDkJFLS0b59q7cmW0EprwfcfUA8d9dg"></script>
    <script src="{{url('/')}}/js/jquery.mb.YTPlayer.js"></script>
    


    


</body>
</html>
