<!doctype html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Lidiane Barbosa</title>
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Raleway:100,300,400,600,700" rel="stylesheet" type="text/css">
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">

    <!-- Styles -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/css/bootstrap.min.css"
          integrity="sha384-PsH8R72JQ3SOdhVi3uxftmaW6Vc51MKb0q5P2rRUpPvrszuE4W1povHYgTpBfshb" crossorigin="anonymous">

    <link rel="stylesheet" href="{{asset('css/style.css')}}">
   
   
</head>
<body>
<section>

    <div class="container">
                          <!-- Video BG Init -->
     
            <!--<div class="row font-times pt-4">
                
                    <div class="col-sm-12 col-md-4 col-lg-4 title-logo">
                    <a class="link-nav" href="#">LOREM IPSUM <span class="sr-only">(current)</span></a>
                    </div>
                    
                    

                    <div class="col-sm-12 col-md-4 col-lg-4 text-center img-logo">
                        <img src="{{asset('img/logo_lidianebarbosa_peq.png')}}" alt="Lidiane Barbosa">
                    </div>
                    <div class="col-sm-12 col-md-4 col-lg-4 text-right button-nav">
                        <button class="btn btn-outline-secondary my-2 my-sm-0" type="submit">LOGIN</button>
                    </div>
            </div>-->
            <nav class="navbar navbar-expand-lg navbar-light font-times">
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarTogglerDemo02"
                    aria-controls="navbarTogglerDemo02" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>

            <div class="collapse navbar-collapse" id="navbarTogglerDemo02">
                <ul class="navbar-nav col-md-4">
                    <li class="nav-item">
                        <a class="nav-link" href="#">LOREM IPSUM <span class="sr-only">(current)</span></a>
                    </li>
                </ul>
                <a class="navbar-brand col-md-4 text-center" href="#">
                    <img src="{{asset('img/logo_lidianebarbosa_peq.png')}}" alt="Lidiane Barbosa">
                </a>
                <ul class="navbar-nav justify-content-end col-md-4">
                    <li class="nav-item">
                         <a href="{{ url('/')}}/login" class="btn btn-outline-secondary my-2 my-sm-0">LOGIN</a>
                    </li>
                </ul>
            </div>
        </nav>



      
            
</section>  

<section>
<div class="container">
    <div class="row">
        <div class="col-md-10 col-center mt-5 p-5">

                    <br>
                        <button class="btn btn-facebook btn-lg btn-block "> <i class="fa fa-facebook" aria-hidden="true"></i> cadastrar com o FACEBOOK</button>
                            <br><br>
                            <div class="titulo">
                              <span>
                                OU
                              </span>
                            </div><br>


                    <h4 class="color-brown"><b>1. DADOS PESSOAIS</b></h4>
                    
                    <form class="form-horizontal" method="POST" action="{{ route('register') }}">
                        {{ csrf_field() }}
                        <div class="row">
                            <div class=" col-6 form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                            <label for="name" class=" control-label">Nome</label>

                           
                                <input id="name" type="text" class="form-control" name="name" value="{{ old('name') }}" required autofocus>

                                @if ($errors->has('name'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                @endif
                               
                            </div>

                            <div class="col-6  form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                                <label for="email" class=" control-label">E-Mail</label>

                                <div class="">
                                    <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required>

                                    @if ($errors->has('email'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('email') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>
                        </div>
                        
                        <div class="row">
                            <div class="col-6 form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                                <label for="password" class="control-label">Senha</label>

                               
                                    <input id="password" type="password" class="form-control" name="password" required>

                                    @if ($errors->has('password'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('password') }}</strong>
                                        </span>
                                    @endif
                               
                            </div>

                            <div class="col-6 form-group">
                                <label for="password-confirm" class=" control-label">Repetir Senha</label>

                                
                                    <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required>
                                
                            </div>

                        </div>
                        <div class="row">
                            <div class="col-6 form-group">
                                <label class=" control-label">Sexo</label>

                                   <select class="form-control" name="sexo">
                                      <option>-- SELECIONE --</option>
                                      <option>MASCULINO</option>
                                      <option>FEMENINO</option>
                                     
                                    </select>
                                
                            </div>
                            <div class="col-6 form-group">
                                <label class=" control-label">Data Nascimento</label>

                                  <input type="text" class="form-control" name="dataNascimento" required>
                                
                            </div>
                        </div>


            
                        <div class="row">
                            <div class="col-6 form-group">
                                <label class=" control-label">Celular</label>

                                   <input type="text" class="form-control" name="celular">
                                
                            </div>
                            <div class="col-6 form-group">
                                <label class=" control-label">CPF</label>
                                  <input type="text" class="form-control" name="cpf">
                            </div>
                        </div>

                        <h4 class="color-brown"><b>2. ENDEREÇO</b></h4>
                        
                        <div class="row">
                            <div class="col-6 form-group">
                                <label class=" control-label">CEP</label>

                                   <input type="text" class="form-control" name="cep">
                                
                            </div>
                            <div class="col-12 form-group">
                                <label class=" control-label">Endereço</label>
                                  <input type="text" class="form-control" name="endereco">
                            </div>
                        </div>

                         <div class="row">
                            <div class="col-6 form-group">
                                <label class=" control-label">Bairro</label>

                                   <input type="text" class="form-control" name="bairro">
                                
                            </div>
                            <div class="col-4 form-group">
                                <label class=" control-label">Número</label>
                                  <input type="text" class="form-control" name="numero">
                            </div>
                        </div>

                         <div class="row">
                            <div class="col-6 form-group">
                                <label class=" control-label">Estado</label>

                                     <select class="form-control" id="estado" name="estado">
                                      <option>-- SELECIONE --</option>
                                      @forelse ($estado as $uf)
                                        <option value="{{ $uf->ufMunicipio }}">{{ $uf->ufMunicipio }}</option>
                                    @empty
                                       <option>nada</option>
                                    @endforelse
                                     
                                    </select>
                                
                            </div>
                            <div class="col-6 form-group">
                                <label class=" control-label">Cidade</label>
                                    <select class="form-control" id="cidade" name="cidade">
                                      <option>-- SELECIONE UM ESTADO PRIMEIRO--</option>
                                    </select>
                            </div>
                        </div>
                        
                        <br><br>
                        <h4 class="color-brown"><b>3. INFORMAÇÕES DE PAGAMENTO</b></h4>

                        <div class="row p-2">
                            <div class="col-12 box-preco text-center ">
                                <br>
                                <p>R$ 99,00 por mês.</p>
                            </div>
                        </div>

                        <br><br>

                        <div class="row">
                             <div class="col-6 form-group">
                                <label class=" control-label">Número do Cartão</label>

                                  <input type="text" class="form-control" name="numeroCartao" required>
                                
                            </div>
                            <div class="col-6 form-group">
                                <label class=" control-label">Bandeira</label>

                                   <select class="form-control" name="bandeira">
                                      <option value="-1">Escolha a bandeira</option>
                                      <option value="visa">Visa</option>
                                      <option value="mastercard">MasterCard</option>
                                      <option value="amex">American Express</option>
                                      <option value="elo">Elo</option>
                                     
                                    </select>
                                
                            </div>
                           
                        </div>



                         <div class="row">
                            <div class="col-4 form-group">
                             
                                <label class=" control-label">VALIDADE MÊS</label>

                                     <select class="form-control" name="mes">
                                      <option>-- VALIDADE MÊS --</option>
                                       @for( $i = 01; $i <= 12; $i++)

                                      <option value="{{ $i }}">{{$i}}</option>

                                      @endfor
                                     
                                    </select>
                                
                            </div>
                            <div class="col-4 form-group">
                                <label class=" control-label">VALIDADE ANO</label>
                                    <select class="form-control" id="exampleFormControlSelect1" name="ano">
                                      <option>-- VALIDADE ANO --</option>
                                      
                                      @for( $i = 2016; $i <= 2032; $i++)

                                      <option value="{{ $i }}">{{$i}}</option>

                                      @endfor
                                     
                                    </select>
                            </div>
                            <div class="col-4 form-group">
                                <label class=" control-label">CÓDIGO DE SEGURANÇA</label>
                                  <input type="text" class="form-control" name="codigo">  
                            </div>
                        </div>



                        <br><br>


                        <div class="form-group">
                            <div class="col-md-12 col-md-offset-4">
                                <button type="submit" class="btn btn-success rounded btn-lg btn-block">
                                    QUERO CONTRATAR O SERVIÇO
                                </button>
                            </div>
                        </div>
                    </form>
       
    </div>
</div>





</section>

<footer>
    <div class="container">
        <div class="row text-center">
            <div class="col-md-6 col-center">
                <ul class="menu-footer ">
                    <li class="border-gray-right"><a href="#">HOME</a></li>
                    <li class="border-gray-right"><a href="#">CONTATO</a></li>
                    <li class="border-gray-right"><a href="#">TERMOS</a></li>
                    <li class="border-gray-right"><a href="#">PRIVACIDADE</a></li>
                    <li><a href="#">COMPRE AGORA</a></li>
                </ul>
            </div>
            <div class="col-12 mt-5 color-gray">
                <p>DESENVOLVIDO COM <img src="{{ url('/')}}/img/heart.png" alt="AMOR"> POR GRUPO GLADIUM</p>
            </div>
            

            
        </div>
    </div>
</footer>


 <!-- Scripts -->
<script src="http://ajax.googleapis.com/ajax/libs/jquery/3.0.0/jquery.min.js"></script>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.3/umd/popper.min.js"
            integrity="sha384-vFJXuSJphROIrBnz7yo7oB41mKfc8JzQZiCq4NCceLEaO4IHwicKwpJf9c9IpFgh"
            crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/js/bootstrap.min.js"
            integrity="sha384-alpBpkh1PFOepccYVYDB4do5UnbKysX5WZXm3XxPqe5iKTfUKjNkCk9SaVuEZflJ"
            crossorigin="anonymous"></script>
            <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAZsDkJFLS0b59q7cmW0EprwfcfUA8d9dg"></script>





<script>

$('#estado').on('change', function(){

    $y = $(this).val();
         // alert($y);


    $.ajax({


              headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
 
             url: "ajaxEndereco/"+$y,
             type: 'GET',
             dataType: 'json',
             success: function(data)
             {

                console.log(data);

                $.each(data, function(i, item) {
                    $('#cidade').append('<option value="'+item.idMunicipio+'">'+item.descricaoMunicipio+'</option>');
                })

                 // data.each(function(value) {
                 //    console.log(value);
                 //    $('#cidade').append('<option>'+value+'</option>');
                 // });
             }





         });
});



 
</script>

    


</body>
</html>


