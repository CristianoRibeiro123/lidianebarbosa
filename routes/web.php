<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



Route::get('/', function () {
    return view('index');
});




Route::get('/pagseguro/session', function () {
    return \PagSeguro::startSession();
});
Route::get('/pagseguro/javascript', function () {
    return response()->make(file_get_contents(\PagSeguro::getUrl()['javascript']), '200')->header('Content-Type', 'text/javascript');
});


Route::get('/login','CursoController@login');
Route::post('send','CursoController@send');


Route::get('/entrar/facebook','CursoController@entrarFaccebook');
Route::get('/retorno/facebook','CursoController@retornoFaccebook');


Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');




Route::get('/ajaxEndereco/{id}', 'CursoController@ajaxEndereco');



