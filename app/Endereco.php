<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Endereco extends Model
{
    //
    //
    protected $table = "enderecos";

    protected $primaryKey = "idendereco";

    protected $fillable = ['cep','logradouro','numero','complemento','bairro','idpessoas','idMunicipio'];


}
